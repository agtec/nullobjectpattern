package buttons;

public class ButtonsFactory {

    public UIComponent createButton(String buttonType) {
        if ("buttons.Button".equalsIgnoreCase(buttonType)) {
            return new Button();
        } else if ("buttons.CheckBox".equalsIgnoreCase(buttonType)) {
            return new CheckBox();
        }
        return new NullObject();
    }

}
