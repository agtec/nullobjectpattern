package buttons;

public class Main {

    public static void main(String[] args) {

        ButtonsFactory buttonsFactory = new ButtonsFactory();
        String buttons[] = {"buttons.Button", "Checkbox", null};
        for (String buttonName: buttons) {
            UIComponent uiComponent = buttonsFactory.createButton(buttonName);
            uiComponent.onClick();
        }
    }
}
