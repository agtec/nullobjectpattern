package buttons;

public class Button implements UIComponent {
    @Override
    public void onClick() {
        System.out.println("Click button!");
    }
}
