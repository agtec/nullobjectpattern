package buttons;

public class NullObject implements UIComponent {
    @Override
    public void onClick() {
        System.out.println("This is null!");
    }
}
